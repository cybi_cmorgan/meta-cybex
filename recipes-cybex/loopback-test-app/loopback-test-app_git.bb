DESCRIPTION = "loopbackTestApp python loopback test application"
HOMEPAGE = "http://www.cybexintl.com"
SECTION = "console/utils"

LICENSE = "cybex"
LICENSE_FLAGS = "cybex"
#TODO: put a proper license file in place and update this
# path to it once it exists in the repository
LIC_FILES_CHKSUM = "file://../../README.txt;md5=de7cb7b7da0fc569dd7376422437989b"

DEPENDS = "python"

APP_DIR = "loopbackTestApp"

FILES_${PN} = "\
  ${bindir}/loopbackTestApp.sh \
  ${datadir}/${APP_DIR}/statistics.py \
  ${datadir}/${APP_DIR}/testGui.py \
  ${datadir}/${APP_DIR}/loopback.py \
  ${datadir}/${APP_DIR}/lbSocket.py \
  ${datadir}/${APP_DIR}/customArgumentParser.py \
  ${datadir}/${APP_DIR}/logic.py \
  ${datadir}/${APP_DIR}/parseArgs.py \
  "

SRC_URI = "gitsm://git@bitbucket.org/cybi_buildmgr/ikabit.git;protocol=ssh"
SRCREV = "${AUTOREV}"

S = "${WORKDIR}/git/tools/loopbackTestApp"

inherit cmake
