DESCRIPTION = "libftdi is a library to talk to FTDI chips.\
FT232BM/245BM, FT2232C/D and FT232/245R using libusb,\
including the popular bitbang mode."
HOMEPAGE = "http://www.intra2net.com/en/developer/libftdi/"
SECTION = "libs"

LICENSE = "LGPLv2"
LIC_FILES_CHKSUM = "file://COPYING.LIB;md5=db979804f025cf55aabec7129cb671ed"

DEPENDS = "virtual/libusb0"
DEPENDS_virtclass-native = "virtual/libusb0-native"

SRC_URI = "http://www.intra2net.com/en/developer/libftdi/download/libftdi1-${PV}.tar.bz2"
SRC_URI[md5sum] = "b79a6356978aa8e69f8eecc3a720ff79"
SRC_URI[sha256sum] = "c0b1af1a13e2c6682a1d8041e5b164a1e0d90267cd378bb51e059bd62f821e21"

# NOTE: LibFTDI1Config.cmake is fixed up below and this fixup makes it
# unsuitable for building on target
FILES_${PN}-dev = "\
    ${libdir}/libftdi1.so \
    ${libdir}/libftdipp1.so \
    ${includedir}/libftdi1/ftdi.h \
    ${includedir}/libftdi1/ftdi.hpp \
    ${libdir}/cmake/libftdi1/LibFTDI1Config.cmake \
    ${libdir}/cmake/libftdi1/UseLibFTDI1.cmake \
    ${bindir}/libftdi1-config \
    ${libdir}/pkgconfig/libftdipp1.pc \
    ${libdir}/pkgconfig/libftdi1.pc \
"

# Fix up path in LibFTDI1Config.cmake file
#
# When libftdi is being built the sysroot path to UseLibFTDI1.cmake is being used
# This is typically something like /usr/lib/cmake/
# This doesn't work well when building on the host however, because sysroot is
# actually at another location, like /home/someuser/yocto_build/x/y/x, so when
# the user of libfti does an include() on that path cmake looks for UseLibFTDI1.cmake
# at /usr/lib/cmake and not ${sysroot}/usr/lib/cmake where the file actually was
# installed. Work around the issue to fix the host building issue by replacing
# paths in the LibFTDI1Config.cmake file. This breaks building on target but
# that isn't a case we need right now.
do_install_append() {
    sed -i "/LIBFTDI_USE_FILE/s;${libdir};${SYSROOT_DESTDIR}${libdir};" ${D}${libdir}/cmake/libftdi1/LibFTDI1Config.cmake
    sed -i "/LIBFTDI_INCLUDE_DIR/s;${includedir};${SYSROOT_DESTDIR}${includedir};" ${D}${libdir}/cmake/libftdi1/LibFTDI1Config.cmake
    sed -i "/LIBFTDI_LIBRARY/s;${libdir}/libftdi;${SYSROOT_DESTDIR}${libdir}/libftdi;" ${D}${libdir}/cmake/libftdi1/LibFTDI1Config.cmake
    sed -i "/LIBFTDI_LIBRARIES/s;${libdir}/libftdi;${SYSROOT_DESTDIR}${libdir}/libftdi;" ${D}${libdir}/cmake/libftdi1/LibFTDI1Config.cmake
}

inherit cmake
