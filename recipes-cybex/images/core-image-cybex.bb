SUMMARY = "Custom Cybex image"

IMAGE_FEATURES += "splash package-management ssh-server-dropbear hwcodecs"

LICENSE = "MIT"

inherit core-image

IMAGE_INSTALL += " \
	eibcontrol \
	cprouter \
	writeread \
	loopback-test-app \
	loopback-system-test \
	beeperdeviceproxysystemtest \
	fandeviceproxysystemtest \
	dfu-util \
	systemd-analyze \
	connman \
"

PREFERRED-VERSION_dfu-util = "0.7"
