DESCRIPTION = "Interface library to interact with ftdi mpsse module"
HOMEPAGE = "https://github.com/chmorgan/libmpsse"
SECTION = "libs"

LICENSE = "LGPLv2"
LIC_FILES_CHKSUM = "file://README.md;md5=8dccfc9ae886be13405c8edfbf0993fb "

DEPENDS = "libftdi1"

SRC_URI = "git://github.com/chmorgan/libmpsse.git;protocol=https;branch=cmake_wip"
SRCREV = "ca80e18047c223e2e9f0ea6618c7e09a9769ad76"

S = "${WORKDIR}/git"

# NOTE: LibMPSSEConfig.cmake is fixed up below and this fixup makes it
# unsuitable for building on target
FILES_${PN}-dev += " \
  ${libdir}/cmake/libmpsse/LibMPSSEConfig.cmake \
  ${libdir}/cmake/libmpsse/UseLibMPSSE.cmake \
"

# Fix up path in LibMPSSEConfig.cmake file
#
# When libmpsse is being built the sysroot path to UseLibMPSSE.cmake is being used
# This is typically something like /usr/lib/cmake/
# This doesn't work well when building on the host however, because sysroot is
# actually at another location, like /home/someuser/yocto_build/x/y/x, so when
# the user of libfti does an include() on that path cmake looks for UseLibMPSSE.cmake
# at /usr/lib/cmake and not ${sysroot}/usr/lib/cmake where the file actually was
# installed. Work around the issue to fix the host building issue by replacing
# paths in the LibMPSSEConfig.cmake file. This breaks building on target but
# that isn't a case we need right now.
do_install_append() {
    sed -i "/LIBMPSSE_USE_FILE/s;${libdir};${SYSROOT_DESTDIR}${libdir};" ${D}${libdir}/cmake/libmpsse/LibMPSSEConfig.cmake
    sed -i "/LIBMPSSE_INCLUDE_DIR/s;${includedir};${SYSROOT_DESTDIR}${includedir};" ${D}${libdir}/cmake/libmpsse/LibMPSSEConfig.cmake
    sed -i "/LIBMPSSE_LIBRARIES/s;${libdir}/libftdi;${SYSROOT_DESTDIR}${libdir}/libftdi;" ${D}${libdir}/cmake/libmpsse/LibMPSSEConfig.cmake
}

inherit cmake
