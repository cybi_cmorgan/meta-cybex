DESCRIPTION = "Fan test application"
HOMEPAGE = "http://www.cybexintl.com"
SECTION = "console/utils"

LICENSE = "cybex"
LICENSE_FLAGS = "cybex"
#TODO: put a proper license file in place and update this
# path to it once it exists in the repository
LIC_FILES_CHKSUM = "file://../../../README.txt;md5=de7cb7b7da0fc569dd7376422437989b"

RDEPENDS_${PN} = "mono"

SRC_URI = "gitsm://git@bitbucket.org/cybi_buildmgr/ikabit.git;protocol=ssh"
SRCREV = "${AUTOREV}"

S = "${WORKDIR}/git/tests/communication_system/FanDeviceProxySystemTest"

inherit cmake
