DESCRIPTION = "loopbackSystemTest system test application"
HOMEPAGE = "http://www.cybexintl.com"
SECTION = "console/utils"

LICENSE = "cybex"
LICENSE_FLAGS = "cybex"
#TODO: put a proper license file in place and update this
# path to it once it exists in the repository
LIC_FILES_CHKSUM = "file://../../../README.txt;md5=de7cb7b7da0fc569dd7376422437989b"

DEPENDS = "loopback-test-app"

# NOTE: Config file location copied from Ikabit high level CMakeLists.txt setting
config_file_location = "${datadir}/loopbackSystemTest"

FILES_${PN} = "\
  ${bindir}/loopbackSystemTest.sh \
  ${config_file_location}/server_config.conf \
"

SRC_URI = "gitsm://git@bitbucket.org/cybi_buildmgr/ikabit.git;protocol=ssh"
SRCREV = "${AUTOREV}"

S = "${WORKDIR}/git/tests/communication_system/loopbackSystemTest"

EXTRA_OECMAKE = "-DCONFIG_FILE_LOCATION=${config_file_location}"

inherit cmake
